package com.company.declaration.controller;

import com.company.declaration.dto.UserDTO;
import com.company.declaration.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/user")
public class UserController {
    private static final String ENTITY_NAME = "user";
    private final UserService service;

    @PostMapping()
    public ResponseEntity<UserDTO> post(@RequestBody @Valid UserDTO userDTO) {
        log.debug("Rest request to create " + ENTITY_NAME + " dto: {}", userDTO);
        return ResponseEntity.ok(service.post(userDTO));
    }

    @GetMapping()
    public ResponseEntity<List<UserDTO>> getAll() {
        log.debug("Rest request to getAll " + ENTITY_NAME);
        return ResponseEntity.ok(service.get());
    }

    @PutMapping("")
    public ResponseEntity<UserDTO> update(@RequestBody @Valid UserDTO userDTO) {
        log.debug("Rest request to update " + ENTITY_NAME + " id: {}", userDTO);
        return ResponseEntity.ok(service.put(userDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable(value = "id") Long id) {
        log.debug("Rest request to delete " + ENTITY_NAME + " id: {}", id);
        return ResponseEntity.ok(service.delete(id));
    }

}
