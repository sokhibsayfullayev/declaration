package com.company.declaration.controller;

import com.company.declaration.dto.ContractPrivilegeDTO;
import com.company.declaration.service.ContractPrivilegeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Log4j2
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/contract-privilege")
public class ContractPrivilegeController {
    private static final String ENTITY_NAME = "contractPrivilege";
    private final ContractPrivilegeService service;

    @PostMapping()
    public ResponseEntity<?> post(@RequestParam("userId") Long userId,
                                  @RequestParam("organizationId") Long organizationId,
                                  @RequestBody ContractPrivilegeDTO contractPrivilegeDTO){
        log.debug("Rest request to create " + ENTITY_NAME + " dto {}",contractPrivilegeDTO);
        return ResponseEntity.ok(service.post(userId,organizationId,contractPrivilegeDTO));
    }


}
