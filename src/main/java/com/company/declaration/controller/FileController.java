package com.company.declaration.controller;

import com.company.declaration.dto.FileDTO;
import com.company.declaration.service.FileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Log4j2
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/files")
public class FileController {
    private static final String ENTITY_NAME = "File";
    private final FileService fileService;
    @Value("${spring.application.name}")
    protected String applicationName;

    @PostMapping
    public ResponseEntity<FileDTO> uploadFile(@RequestParam("contractId") Long contract,
                                              @RequestParam("files") MultipartFile files) {
        log.debug("Rest request to save File: {}", files);

        return ResponseEntity.ok(fileService.saveFiles(contract, files));
    }

    @GetMapping
    public ResponseEntity<?> getFile(HttpServletResponse response, @RequestParam("contractId") Long contractId) throws IOException {
        log.debug("Rest request to get File with contract ID: {}", contractId);

        return ResponseEntity.ok(fileService.getFilesByContractId(response, contractId));
    }
}
