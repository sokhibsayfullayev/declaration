package com.company.declaration.controller.handler;

import jakarta.validation.ValidationException;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        String message = e.getMessage();

        if (e instanceof ConfigDataResourceNotFoundException) {
            status = HttpStatus.NOT_FOUND;
            message = "Resource not found";
        } else if (e instanceof ValidationException) {
            status = HttpStatus.BAD_REQUEST;
            message = "Validation failed";
        }

        return ResponseEntity.status(status).body(message);
    }
}