package com.company.declaration.service.impl;

import com.company.declaration.dto.OrganizationDTO;
import com.company.declaration.service.OrganizationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {
    @Override
    public OrganizationDTO post(OrganizationDTO OrganizationDTO) {
        return null;
    }

    @Override
    public List<OrganizationDTO> get() {
        return null;
    }

    @Override
    public OrganizationDTO put(Long id) {
        return null;
    }

    @Override
    public OrganizationDTO delete(Long id) {
        return null;
    }
}
