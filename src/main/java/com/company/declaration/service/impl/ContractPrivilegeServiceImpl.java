package com.company.declaration.service.impl;

import com.company.declaration.domain.ContractPrivilegeEntity;
import com.company.declaration.domain.OrganizationEntity;
import com.company.declaration.domain.UserEntity;
import com.company.declaration.dto.ContractPrivilegeDTO;
import com.company.declaration.exception.ItemNotFoundException;
import com.company.declaration.mapper.ContractPrivilegeMapper;
import com.company.declaration.repository.ContractPrivilegeRepository;
import com.company.declaration.repository.OrganizationRepository;
import com.company.declaration.repository.UserRepository;
import com.company.declaration.service.ContractPrivilegeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class ContractPrivilegeServiceImpl implements ContractPrivilegeService {
    private final UserRepository userRepository;
    private final OrganizationRepository organizationRepository;
    private final ContractPrivilegeMapper mapper;
    private final ContractPrivilegeRepository repository;

    @Override
    public ContractPrivilegeDTO post(Long userId, Long organizationId, ContractPrivilegeDTO contractPrivilegeDTO) {
        UserEntity userEntity = userRepository.findById(userId).orElseThrow(
                () -> new ItemNotFoundException("User not found with id: " + userId)
        );
        OrganizationEntity organizationEntity = organizationRepository.findById(organizationId).orElseThrow(
                () -> new ItemNotFoundException("Organization not found with id: " + userId)
        );

        ContractPrivilegeEntity contractPrivilegeEntity = mapper.toEntity(contractPrivilegeDTO);
        contractPrivilegeEntity.setOwner(userEntity);
        contractPrivilegeEntity.setOrganization(organizationEntity);
        return mapper.toDto(repository.save(contractPrivilegeEntity));
    }

    @Override
    public List<ContractPrivilegeDTO> get() {
        return null;
    }

    @Override
    public ContractPrivilegeDTO put(Long id) {
        return null;
    }

    @Override
    public ContractPrivilegeDTO delete(Long id) {
        return null;
    }
}
