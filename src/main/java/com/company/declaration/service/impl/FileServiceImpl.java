package com.company.declaration.service.impl;

import com.company.declaration.domain.ContractPrivilegeEntity;
import com.company.declaration.domain.FileEntity;
import com.company.declaration.dto.FileDTO;
import com.company.declaration.exception.ItemNotFoundException;
import com.company.declaration.mapper.FileMapper;
import com.company.declaration.repository.ContractPrivilegeRepository;
import com.company.declaration.repository.FileRepository;
import com.company.declaration.service.FileService;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.aspectj.weaver.patterns.HasMemberTypePatternForPerThisMatching;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private static final List<String> allowedContentTypes = List.of("pdf", "zip");
    private final FileRepository fileRepository;
    private final FileMapper mapper;
    private final ContractPrivilegeRepository contractPrivilegeRepository;

    @Value("${app.upload_path}")
    private Path rootLocation;

    @Override
    public HttpServletResponse getFilesByContractId(HttpServletResponse response, Long contractId) throws IOException {

        List<FileEntity> allByContractId = fileRepository.findAllByContractId(contractId);
        List<String> paths = allByContractId.stream().map(FileEntity::getFilePath).toList();

        for (String path : paths) {
            try {
                File file = new File(path);
                System.out.println(file);
                InputStream inputStream = new FileInputStream(file);
                response.setContentLength((int) file.length());
                response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");

                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    response.getOutputStream().write(buffer, 0, bytesRead);
                }


                inputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return response;
    }

    @Override
    public FileDTO saveFiles(Long contractId, MultipartFile files) {
        log.debug("Request to save File: {}", files);
        String file = StringUtils.cleanPath(Objects.requireNonNull(files.getOriginalFilename()));
        String fileName = file.substring(0, file.indexOf("."));
        ContractPrivilegeEntity contractPrivilegeEntity = contractPrivilegeRepository.findById(contractId).orElseThrow(
                () -> new ItemNotFoundException("Contract id is not available id:" + contractId)
        );

        if (files.isEmpty() || file.contains("..") || !allowedContentTypes.contains(FilenameUtils.getExtension(file))) {
            throw new RuntimeException("Cannot store this type of file allowed .pdf or .zip your type:" + file);
        }

        FileEntity entity = new FileEntity();
        entity.setFileName(fileName + "." + FilenameUtils.getExtension(file));
        entity.setFileType(files.getContentType());
        entity.setContract(contractPrivilegeEntity);
        entity.setFilePath(String.format("%s", LocalDate.now().format(getFormatter()) + "/" + file));
        fileRepository.save(entity);


        try (InputStream in = files.getInputStream()) {
            saveFileToDisk(in, entity);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return mapper.toDto(entity);
    }

    private void saveFileToDisk(InputStream inputStream, FileEntity entity) throws IOException {
        Path dir = getFileDir(entity);
        if (!Files.exists(dir)) {
            Files.createDirectories(dir);
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(dir + "/" + entity.getFileName());

            byte[] data = inputStream.readAllBytes();
            outputStream.write(data);

            log.debug("File successfully uploaded");
            outputStream.close();
        } catch (IOException | RuntimeException e) {
            throw new RuntimeException(e);
        }
    }

    private Path getFileDir(FileEntity entity) {
        assert entity.getId() != null;
        Path path = Paths.get(".");
        return path
                .resolve(getFormatter().format(entity.getCreatedAt()));
    }

    private DateTimeFormatter getFormatter() {
        return DateTimeFormatter.ofPattern("yyyy/MM/dd")
                .withZone(ZoneOffset.UTC);
    }
}
