package com.company.declaration.service.impl;

import com.company.declaration.domain.UserEntity;
import com.company.declaration.dto.UserDTO;
import com.company.declaration.exception.ItemNotFoundException;
import com.company.declaration.mapper.UserMapper;
import com.company.declaration.repository.UserRepository;
import com.company.declaration.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final UserMapper mapper;


    @Override
    public UserDTO post(UserDTO userDTO) {
        log.debug("Request to post User: {}", userDTO);

        final var entity = mapper.toEntity(userDTO);
        repository.saveAndFlush(entity);

        return mapper.toDto(entity);
    }

    @Override
    public List<UserDTO> get() {
        log.debug("Request to get");

        return mapper.toDto(repository.findAll());
    }

    @Override
    public UserDTO put(UserDTO userDTO) {
        log.debug("Request to put dto: {}", userDTO);

        UserEntity userEntity = repository.findById(userDTO.getId()).orElseThrow(
                () -> new ItemNotFoundException("User not Found with this id:" + userDTO.getId())
        );
        var formData = mapper.toEntity(userDTO);
        formData.setId(userEntity.getId());

        return mapper.toDto(repository.saveAndFlush(formData));
    }

    @Override
    public UserDTO delete(Long id) {
        log.debug("Request to post ID: {}", id);
        UserEntity userEntity = repository.findById(id).orElseThrow(
                () -> new ItemNotFoundException("User not Found with this id:" + id)
        );
        repository.delete(userEntity);
        return mapper.toDto(userEntity);
    }
}
