package com.company.declaration.service;

import com.company.declaration.dto.FileDTO;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


public interface FileService {
    HttpServletResponse getFilesByContractId(HttpServletResponse response,Long contractId) throws IOException;

    FileDTO saveFiles(Long contractId, MultipartFile files);
}
