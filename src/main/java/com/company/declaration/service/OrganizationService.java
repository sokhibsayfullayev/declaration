package com.company.declaration.service;

import com.company.declaration.dto.OrganizationDTO;

import java.util.List;

public interface OrganizationService {
    String SERVICE_NAME = "OrganizationService";
    
    OrganizationDTO post(OrganizationDTO OrganizationDTO);
    
    List<OrganizationDTO> get();
    
    OrganizationDTO put(Long id);
    
    OrganizationDTO delete(Long id);
}
