package com.company.declaration.service;

import com.company.declaration.dto.UserDTO;

import java.util.List;

public interface UserService {
    String SERVICE_NAME = "PupilService";

    UserDTO post(UserDTO userDTO);

    List<UserDTO> get();

    UserDTO put(UserDTO userDTO);

    UserDTO delete(Long id);
}
