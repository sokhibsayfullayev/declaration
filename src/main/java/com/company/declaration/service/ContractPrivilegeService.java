package com.company.declaration.service;

import com.company.declaration.dto.ContractPrivilegeDTO;

import java.util.List;

public interface ContractPrivilegeService {
    String SERVICE_NAME = "ContractPrivilegeService";

    ContractPrivilegeDTO post(Long userId, Long organizationId, ContractPrivilegeDTO ContractPrivilegeDTO);

    List<ContractPrivilegeDTO> get();

    ContractPrivilegeDTO put(Long id);

    ContractPrivilegeDTO delete(Long id);
}
