package com.company.declaration.domain;

import com.company.declaration.domain.enums.ReportType;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;


@Entity
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "contract_privilege")
public class ContractPrivilegeEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName="sequence_generator", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    protected Long id;

    /**
     * Hujjat egasi
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id",updatable = false,nullable = false)
    private UserEntity owner;

    /**
     * Korxona
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization",updatable = false,nullable = false)
    private OrganizationEntity organization;

    /**
     * Shartnoma sanasi
     */
    @Column(name = "date",nullable = false)
    private LocalDate date;


    /**
     * Shartnoma raqami
     */
    @Column(name = "number",nullable = false)
    private String number;

    /**
     * Yil
     */
    @Column(name = "year",nullable = false)
    private String year;

    /**
     * Hisobot turi
     */
    @Column(name = "report_type",nullable = false)
    private ReportType reportType;

    /**
     * Jami yo'naltirilgn summa
     */
    @Column(name = "general_sum",nullable = false)
    private BigDecimal generalSum;

    @OneToMany(mappedBy = "contract")
    private Set<FileEntity> files;
}
