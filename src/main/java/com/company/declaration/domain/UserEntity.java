package com.company.declaration.domain;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Entity
@Table(name = "users")
@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName="sequence_generator", allocationSize = 1)
    @Column(name = "id", updatable = false, nullable = false)
    protected Long id;
    /**
      * JShShIR
     **/
    @Column(name = "pin",unique = true)
    private String pin;

    /**
     * F.I.O
     */
    @Column(name = "fullName",nullable = false)
    private String fullName;

    /**
     * Daromad manbai
     */
    @Column(name = "incomeSource")
    private String incomeSource;


}
