package com.company.declaration.exception;

public class ItemNotFoundException extends RuntimeException{
    public ItemNotFoundException(String message){super(message);}
}
