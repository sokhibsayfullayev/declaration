package com.company.declaration.mapper;

import com.company.declaration.domain.FileEntity;
import com.company.declaration.dto.FileDTO;
import org.mapstruct.Mapper;

@Mapper(
        config = IgnoreUnmappedMapperConfig.class,
        componentModel = "spring",
        uses = {
                ContractPrivilegeMapper.class
        }
)
public interface FileMapper extends EntityMapper<FileDTO, FileEntity> {

}
