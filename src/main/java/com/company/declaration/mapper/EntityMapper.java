package com.company.declaration.mapper;

import org.mapstruct.*;

import java.util.List;
import java.util.Set;

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface EntityMapper<D, E> {

    @Named(value = "toDto")
    D toDto(E entity);

    @Named(value = "toEntity")
    E toEntity(D dto);

    @IterableMapping(qualifiedByName = "toDto")
    List<D> toDto(List<E> entityList);

    @IterableMapping(qualifiedByName = "toEntity")
    List<E> toEntity(List<D> dtoList);

}
