package com.company.declaration.mapper;

import com.company.declaration.domain.UserEntity;
import com.company.declaration.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(
        config = IgnoreUnmappedMapperConfig.class,
        componentModel = "spring"
)
public interface UserMapper extends EntityMapper<UserDTO, UserEntity> {

}
