package com.company.declaration.mapper;

import com.company.declaration.domain.ContractPrivilegeEntity;
import com.company.declaration.dto.ContractPrivilegeDTO;
import org.mapstruct.Mapper;

@Mapper(
        config = IgnoreUnmappedMapperConfig.class,
        componentModel = "spring",
        uses = {
                UserMapper.class,
                OrganizationMapper.class
        }
)
public interface ContractPrivilegeMapper extends EntityMapper<ContractPrivilegeDTO, ContractPrivilegeEntity> {

}
