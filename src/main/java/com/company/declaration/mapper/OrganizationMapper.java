package com.company.declaration.mapper;

import com.company.declaration.domain.OrganizationEntity;
import com.company.declaration.domain.UserEntity;
import com.company.declaration.dto.OrganizationDTO;
import com.company.declaration.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(
        config = IgnoreUnmappedMapperConfig.class,
        componentModel = "spring"
)
public interface OrganizationMapper extends EntityMapper<OrganizationDTO, OrganizationEntity> {

}
