package com.company.declaration.repository;

import com.company.declaration.domain.ContractPrivilegeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractPrivilegeRepository extends JpaRepository<ContractPrivilegeEntity, Long> {

}
