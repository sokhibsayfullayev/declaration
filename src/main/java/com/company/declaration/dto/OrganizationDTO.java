package com.company.declaration.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class OrganizationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * Tashkilot rahbari
     */
    @NotNull
    private UserDTO owner;

    /**
     * Tashkilot nomi
     */
    @NotBlank
    @Length(max = 180)
    private String name;

}
