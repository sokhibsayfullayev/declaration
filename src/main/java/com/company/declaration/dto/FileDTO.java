package com.company.declaration.dto;

import com.company.declaration.domain.ContractPrivilegeEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.Instant;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class FileDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;

    @JsonIgnore
    private Instant createdAt;

    private String fileName;
    private String fileType;
    private String filePath;
    private ContractPrivilegeDTO contract;

    private String baseURL;
}
