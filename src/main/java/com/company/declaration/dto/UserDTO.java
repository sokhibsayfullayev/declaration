package com.company.declaration.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    protected Long id;
    /**
     * JShShIR
     **/
    @Length(max = 50)
    private String pin;

    /**
     * F.I.O
     */
    @NotBlank
    @Length(max = 180)
    private String fullName;

    /**
     * Daromad manbai
     */
    @NotBlank
    @Length(max = 180)
    private String incomeSource;

}
