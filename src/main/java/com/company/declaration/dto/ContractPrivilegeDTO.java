package com.company.declaration.dto;

import com.company.declaration.domain.enums.ReportType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ContractPrivilegeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    protected Long id;
    /**
     * Hujjat egasi
     */
    private UserDTO owner;

    /**
     * Korxona
     */
    private OrganizationDTO organization;

    /**
     * Shartnoma sanasi
     */
    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate date;

    /**
     * Shartnoma raqami
     */
    @NotBlank
    @Max(14)
    private String number;

    /**
     * Yil
     */
    @NotNull
    private String year;

    /**
     * Hisobot turi
     */
    @NotNull
    private ReportType reportType;

    /**
     * Jami yo'naltirilgn summa
     */
    @NotNull
    private BigDecimal generalSum;

}
