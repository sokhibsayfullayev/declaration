ALTER TABLE contract_privilege
    ADD CONSTRAINT contract_unique UNIQUE (owner_id, organization, date);